window.addEventListener('load', function() {
	//stran nalozena
	
	// Izvedi prijavo
	var izvediPrijavo = function() {
		var uporabnik = document .querySelector("#uporabnisko_ime").value;
		document.querySelector("#uporabnik").innerHTML = uporabnik;
		document.querySelector(".pokrivalo").style.visibility = "hidden";
	}
	
	document.querySelector("#prijavniGumb").addEventListener("click", izvediPrijavo);
	onEnterPressed("#prijavniGumb", izvediPrijavo);
		
	var dodajOpomnik = function() {
		var naziv_opomnika = document.querySelector("#naziv_opomnika").value;
		document.querySelector("#naziv_opomnika").value = "";
		var cas_opomnika = document.querySelector("#cas_opomnika").value;
		document.querySelector("#cas_opomnika").value = "";
		
		var opomnik = document.querySelector("#opomniki");
		var opomnikHtml = "\
			<div class='opomnik'> \
			  <div class='naziv_opomnika'>{0}</div> \
			  <div class='cas_opomnika'> Opomnik čez <span>{1}</span> sekund.</div> \
			</div>"
		opomnik.innerHTML += opomnikHtml.format(naziv_opomnika, cas_opomnika);
	}
	
	document.querySelector("#dodajGumb").addEventListener("click", dodajOpomnik);
		
	//Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");
		
		for (i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML);
	
			//TODO: 
			// - če je čas enak 0, izpiši opozorilo "Opomnik!\n\nZadolžitev NAZIV_OPOMNIK je potekla!"
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
		}
	}
	setInterval(posodobiOpomnike, 1000);
	
});


function onEnterPressed(id, listener){
	// TODO
}

// First, checks if it isn't implemented yet.
if (!String.prototype.format) {
  String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) { 
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };
}